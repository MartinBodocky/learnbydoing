﻿using Akka.Persistence;
using GameConsole.ActorModel.Commands;
using GameConsole.ActorModel.Events;
using System;

namespace GameConsole.ActorModel.Actors
{

    class PlayerActorState
    {
        public string PlayerName { get; set; }
        public int Health { get; set; }
        public override string ToString()
        {
            return $"[PlayerActorState {PlayerName} {Health}]";
        }
    }
    class PlayerActor : ReceivePersistentActor
    {
        private PlayerActorState _state;
        private int _eventCount;

        public override string PersistenceId => $"player-{_state.PlayerName}";

        public PlayerActor(string playerName, int startingHealth)
        {
            _state = new PlayerActorState
            {
                PlayerName = playerName,
                Health = startingHealth
            };

            DisplayHelper.WriteLine($"{_state.PlayerName} created");

            Command<HitPlayer>(message => HitPlayer(message));
            Command<DisplayStatus>(message => DisplayPlayerStatus());
            Command<SimulateError>(message => SimulateError());

            Recover<PlayerHit>(message =>
            {
                DisplayHelper.WriteLine($"{_state.PlayerName} replaying Player Hit event {message} from journal");
                _state.Health -= message.DamageTaken;
            });

            Recover<SnapshotOffer>(offer =>
            {
                DisplayHelper.WriteLine($"{_state.PlayerName} getting snapshot offer");
                _state = (PlayerActorState)offer.Snapshot;

            });
        }

        private void HitPlayer(HitPlayer command)
        {
            DisplayHelper.WriteLine($"{_state.PlayerName} received Hit Player command");

            DisplayHelper.WriteLine($"{_state.PlayerName} persisting Hit Player command");

            var @event = new PlayerHit(command.Damage);

            Persist(@event, playerHitEvent =>
            {
                DisplayHelper.WriteLine($"{_state.PlayerName} persisted OK. Player Hit");

                _state.Health -= playerHitEvent.DamageTaken;
                _eventCount++;

                if(_eventCount == 5)
                {
                    DisplayHelper.WriteLine($"{_state.PlayerName} saving snapshot");

                    SaveSnapshot(_state);

                    _eventCount = 0;

                    DisplayHelper.WriteLine($"{_state.PlayerName} reser event count to 0");
                }
            });
        }

        private void DisplayPlayerStatus()
        {
            DisplayHelper.WriteLine($"{_state.PlayerName} received Display Status command");

            Console.WriteLine($"{_state.PlayerName} has {_state.Health} health");
        }

        private void SimulateError()
        {
            DisplayHelper.WriteLine($"{_state.PlayerName} received Simulate error command");

            throw new ApplicationException($"Simulated exception in player: {_state.PlayerName}");
        }
    }
}
