﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameConsole.ActorModel.Events
{
    class PlayerCreated
    {
        public string PlayerName { get; private set; }
        public PlayerCreated(string name)
        {
            PlayerName = name;
        }
    }
}
