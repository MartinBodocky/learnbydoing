﻿namespace GameConsole.ActorModel.Events
{
    class PlayerHit
    {
        public int DamageTaken { get; private set; }

        public PlayerHit(int damage)
        {
            DamageTaken = damage;
        }
    }
}
