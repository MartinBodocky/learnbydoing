﻿using Akka.Configuration.Hocon;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIGoglomatics.Helpers
{
    /// <summary>
    /// Helper to provide settings specified in the globomantics section of HOCON
    /// </summary>
    internal static class GlobomanticsConfiguration
    {
        public static int NumberOfRecommendations { get; } = 5;

        static GlobomanticsConfiguration()
        {
            var section = (AkkaConfigurationSection)ConfigurationManager.GetSection("akka");
            var akkaConfig = section.AkkaConfig;

            var globomantics = akkaConfig.GetConfig("globomantics");
            if (globomantics != null)
                NumberOfRecommendations = globomantics.GetInt("number-of-recommendations", NumberOfRecommendations);
        }
    }
}
